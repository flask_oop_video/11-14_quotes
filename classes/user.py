# import needed modules
import sqlite3
from config import Config
from passlib.hash import sha256_crypt

# create a config object so we can access it's properties
config = Config()

# ---------------------------------------------

class User:

    # this is a contructor class
    # it holds properties of the object that can be 
    # referred to throughout via "self"
    def __init__(self):
        # put properties here

        # path is relative to root of app (app.py)
        # it comes from the config.py class in the root folder
        self.dbPath = config.dbName

    # ---------------------------------------------

    # this method accepts "self" and the user's registration details
    # it checks if a user already exists with that name and, if not, 
    # adds their details as a row in the users table of the database
    # if the user already exists, it throws an error
    def insertUser(self,username,password,email):

        # this is a convenient way to open and close 
        # a database connection. Notice how we've used the self.dbPath
        # property of the user object
        with sqlite3.connect(self.dbPath) as con:

            # creates a cursor (pointer) for our database queries
            cur = con.cursor()
            # executes some SQL to select the user's details
            cur.execute("SELECT * FROM users WHERE username = ?",[username])
            # retrieves this row from the dictionary and assigns the 
            # result to the variable result
            result = cur.fetchone()

            # if the result has returned as True, it means that 
            # the user already exists in the database so we return 
            # False to app.py where this method was called 
            if result:
                return False
            else:
                # if the user doesn't already exist in the database
                # we encrypt the password they've submitted using 
                # the sha256_crypt library
                passwordHashed = sha256_crypt.encrypt(str(password))

                # create cursor
                cur = con.cursor()
                # execute SQL. Notice how we've used ? as placeholders for the actual values. 
                # This is important to prevent SQL injection. The values we want to enter
                # into the database are given as a set in the exact order immediately after.
                cur.execute("INSERT INTO users (username,password,email) VALUES (?,?,?)",(username,passwordHashed,email))
                # this commits the row intot he users table
                con.commit()
                # now we return True to app.py so that we can handle errors/success
                return True

    # ---------------------------------------------

    # this method authenticates the user. It accepts the username
    # and password that they've submitted. Tests that the user exists
    # and that the passwords match and returns True or False to app.py
    def authenticateUser(self,username,passwordAttempt):

        # open and close a database connection
        with sqlite3.connect(self.dbPath) as con:

            # Make the result a dictionary.
            # Dictionaries are very easy to work with as we can access
            # the column names directly. eg: result['username']
            con.row_factory = sqlite3.Row
            # create database cursor
            cur = con.cursor()
            # execute SQL that selects the user's details
            cur.execute("SELECT * FROM users WHERE username = ?",[username])
            # assigns the results to the result variable
            result = cur.fetchone()

            # If we actually found a user with the name submitted
            if result:
                # get the stored hashed password from the db query above
                # since it's a dictionary we can just access the column name
                # as a key. In this case 'password'
                passwordHash = result['password']

                # This convenient method checks that the encrytped password
                # that we have stored in the database for that user matches
                # an encrypted version of what they've just attempted to log in with
                # If it matches we return True, if not we return False.
                if sha256_crypt.verify(passwordAttempt, passwordHash):
                    return result['id']
                else:
                    return False
            else:
                # this else statement is a fallback if the username
                # that was submitted isn't found in the user table in the database
                return False