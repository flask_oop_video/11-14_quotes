# import needed modules
import sqlite3
from config import Config

# create a config object so we can access it's properties
config = Config()

# ---------------------------------------------


class Quote:

    # this is a contructor class
    # it holds properties of the object that can be 
    # referred to throughout via "self"
    def __init__(self):
        # put properties here

        # path is relative to root of app (app.py)
        # it comes from the config.py class in the root folder
        self.dbPath = config.dbName


    def addQuote(self,quote,source,id_users):


        # this is a convenient way to open and close 
        # a database connection. Notice how we've used the self.dbPath
        # property of the user object
        with sqlite3.connect(self.dbPath) as con:

            # create cursor
            cur = con.cursor()
            # execute SQL. Notice how we've used ? as placeholders for the actual values. 
            # This is important to prevent SQL injection. The values we want to enter
            # into the database are given as a set in the exact order immediately after.
            cur.execute("INSERT INTO quotes (quote,source,id_users) VALUES (?,?,?)",(quote,source,id_users))
            # this commits the row intot he users table
            con.commit()
            # now we return True to app.py so that we can handle errors/success
            return True
